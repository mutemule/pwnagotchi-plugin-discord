import logging
import os
import importlib
import re
import random
import requests

import pwnagotchi
import pwnagotchi.utils as utils
import pwnagotchi.plugins as plugins


class Discord(plugins.Plugin):
    __author__ = 'dwg@flargle.io'
    __version__ = '0.8.0'
    __license__ = 'BSD'
    __description__ = 'Post periodic activity updates to Discord.'

    def __init__(self):
        self.ready = False
        self.startup_announced = False
        self.last_post_at = 0
        self.last_update_at = 0

        self.discord_message_id = False
        self.update_message = False

        self.starting_handshakes = -1

        # The stats we care about are only available by querying the ui,
        # so we need to track them ourselves
        self.stats = {
                'deauths': 0,
                'associations': 0,
                'peers': 0
                }

        self.messages = {
                'startup': ['Getting ready to pwn!'],
                'running': ['Pwning! I\'m just pwning alo-ong!'],
                'shutdown': ['pwning done, naptime now']
                }

    def on_loaded(self):
        logging.info('Discord: Starting to load.')

        discord_spec = importlib.util.find_spec('discord')
        if not discord_spec:
            logging.error('Discord: discord.py module not installed, cannot load plugin')
            return

        if 'webhook_url' not in self.options or not self.options['webhook_url']:
            # This should arguably go to the UI
            logging.error('Discord: No Webhook URL specified, cannot post to Discord')
            return

        if 'post_frequency' not in self.options or not self.options['post_frequency']:
            self.options['post_frequency'] = 153
            logging.info('Discord: No post frequency specified, using default value of %d',
                    self.options['post_frequency'])

        if 'update_frequency' not in self.options or not self.options['update_frequency']:
            self.options['update_frequency'] = 12
            logging.info('Discord: No post update frequency specified, using default value of %d',
                    self.options['update_frequency'])

        if 'username' not in self.options or not self.options['username']:
            logging.info('Discord: No username provided, using pwnagotchi name instead')
            self.options['username'] = pwnagotchi.name()

        if 'startup_messages' in self.options:
            self.messages['startup'] = self.options['startup_messages']

        if 'running_messages' in self.options:
            self.messages['running'] = self.options['running_messages']

        if 'shutdown_messages' in self.options:
            self.messages['shutdown'] = self.options['shutdown_messages']

        self.ready = True
        logging.info('Discord: Loaded successfully!')

    def on_internet_available(self, agent):
        logging.debug('Discord: Entering on_internet_available')
        if not self.ready:
            return

        # This is going to be _slightly_ inaccurate if we capture new handshakes
        # in the first three minutes of runtime
        if self.starting_handshakes == -1:
            self.starting_handshakes = self.get_handshakes_count(agent)[1]

        message = 'oops'
        uptime = pwnagotchi.uptime()
        mins_since_last_post = (uptime - self.last_post_at) / 60
        mins_since_last_update = (uptime - self.last_update_at) / 60

        if not self.startup_announced:
            logging.info('Discord: Detected new boot w/Internet, let\'s announce it!')
            message = random.choice(self.messages['startup'])

        elif mins_since_last_post >= self.options['post_frequency']:
            logging.info('Discord: Time to post a new update!')
            message = random.choice(self.messages['running'])
            self.update_message = False

        elif mins_since_last_update >= self.options['update_frequency']:
            logging.info('Discord: Time to update our post!')
            message = random.choice(self.messages['running'])
            self.update_message = True

        else:
            return

        self.send_to_discord(agent, message)
        logging.debug('Discord: Leaving on_internet_available')

    def on_deauthentication(self, *args):
        logging.debug('Discord: incrementing deauth count')
        self.stats['deauths'] += 1

    def on_association(self, *args):
        logging.debug('Discord: incrementing association count')
        self.stats['associations'] += 1

    def on_peer_detected(self, *args):
        logging.debug('Discord: incrementing peer count')
        self.stats['peers'] += 1

    def get_handshakes_count(self, agent):
        logging.debug('Discord: Determining current handshakes')
        shakes = agent.view().get('shakes')

        # This returns 'ses (tot) [SSID]':
        # ses: Total handshakes captures this session
        # tot: Total unique handshakes captured
        # ssid: SSID of the most recently captured handshake
        match = re.search(r'^([0-9]+) \(([0-9]+)\)( \[(.*)\])?', shakes)
        ses = int(match.group(1))
        tot = int(match.group(2))
        ssid = match.group(4)

        logging.debug('Discord: returning shakes: %s:%s:%s', ses, tot, ssid)

        return ses, tot, ssid

    def send_to_discord(self, agent, msg):
        try:
            from discord import Embed, File, RequestsWebhookAdapter, Webhook
            from discord import Forbidden, HTTPException, InvalidArgument, NotFound
        except ImportError as e:
            logging.error('Discord: Unable to load discord.py')
            logging.debug(e)
            return

        display = agent.view()
        url = f'https://pwnagotchi.ai/pwnfile/#!{agent.fingerprint()}'
        uptime = pwnagotchi.uptime()
        pwntime = utils.secs_to_hhmmss(uptime)

        screenshot = '/var/tmp/pwnagotchi/pwnagotchi.png' if os.path.exists(
            '/var/tmp/pwnagotchi') else '/root/pwnagotchi.png'
        display.image().save(screenshot, 'png')

        shakes_ses, shakes_tot, shakes_ssid = self.get_handshakes_count(agent)
        shakes_new = shakes_tot - self.starting_handshakes

        embed = Embed(
                title=msg,
                url=url
                )
        embed.add_field(
                name='Pwntime',
                value=pwntime,
                inline=True
                )
        embed.add_field(
                name='Associations',
                value=self.stats['associations'],
                inline=True
                )
        embed.add_field(
                name='Kicked',
                value=self.stats['deauths'],
                inline=True
                )
        embed.add_field(
                name='Handshakes',
                value=f'{shakes_ses}+{shakes_new} ({shakes_tot})',
                inline=True
                )
        embed.add_field(
                name='Pwnd',
                value=shakes_ssid,
                inline=True
                )
        embed.add_field(
                name='Peers',
                value=self.stats['peers'],
                inline=True
                )
        embed.set_image(url='attachment://pwnagotchi.png')

        try:
            webhook = Webhook.from_url(
                self.options['webhook_url'],
                adapter=RequestsWebhookAdapter()
                )

            if self.update_message and self.discord_message_id:
                logging.info('Discord: Updating existing post')
                webhook.edit_message(self.discord_message_id, embed=embed)
                self.last_update_at = uptime
            else:
                logging.info('Discord: Creating new post')
                if self.discord_message_id:
                    logging.info('Discord: Deleting previous post')
                    webhook.delete_message(self.discord_message_id)

                discord_msg = webhook.send(file=File(screenshot), embed=embed)
                self.discord_message_id = discord_msg['id']
                self.last_post_at = uptime

            logging.info('Discord: Successfully posted: %s', msg)

            self.startup_announced = True
            self.update_message = False
            display.set('status', 'Discord update posted!')
            display.update(force=True)

        except HTTPException as e:
            logging.exception('Discord: Failed, HTTP %d error when posting update: %s',
                    e.status, e.response)
            logging.debug(e)
        except NotFound as e:
            logging.exception('Discord: Failed, unknown webhook URL: %s ', e.response)
            logging.debug(e)
        except Forbidden as e:
            logging.exception('Discord: Failed, invalid webhook URL specified: %s ', e.response)
            logging.debug(e)
        except InvalidArgument as e:
            logging.exception('Discord: Failed, malformed message: %s ', e.response)
            logging.debug(e)
